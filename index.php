<?php

$font_domain = "fonts.example.domain";
$allowed = array('https://www.example.domain'); 
$font_dir = "file/";
$style_dir = "styles/";

$uri = $_SERVER['REQUEST_URI'];
$ua = $_SERVER['HTTP_USER_AGENT'];
$ua_hash = sha1($ua);

if (!file_exists($font_dir)) {
mkdir($font_dir, 0777, true);
}
if (!file_exists($style_dir)) {
mkdir($style_dir, 0777, true);
}

if (substr( $uri, 0, 4 ) === "/css") {

  $url = "https://fonts.googleapis.com" . $uri;

  $font_family = $_GET["family"];

  function getFont($ua_hash) {
    global $ua_dir, $font_family;
    if (file_exists($style_dir . $font_family ."/". $ua_hash)) {
      $file = unserialize(file_get_contents($style_dir . $font_family ."/". $ua_hash));
    }  
  }

  $file_from_disk = getFont($ua_hash);

  if($file_from_disk) {
    echo $file_from_disk;
    exit;
  }

  $options = array(
    'http'=>array(
      'method'=>"GET",
      'header'=>"Accept-language: de,en-US;q=0.7,en;q=0.3\r\n" .
                "User-Agent: $ua \r\n"
    )
  );


  function storeUA($file) {
    global $style_dir, $ua_hash;
    $font_css = fopen($style_dir . $font_family ."/". $ua_hash, "w");
    fwrite($font_css, serialize($file));
    fclose($font_css);
  }
  
  $context = stream_context_create($options);
  $file = file_get_contents($url, false, $context);
  $file = str_replace("fonts.gstatic.com", $font_domain, $file);

  storeUA($file);

  header('Content-type: text/css');
  echo $file;
  exit;

} else {
    
  $localFilePath = end(explode('/', $uri));
  $font_family = explode('/', $uri)[1];
  var_dump($font_family);
  $filePath = $font_dir.$font_family."/".$localFilePath;

    if (!file_exists($filePath)) {

      $requestPath = "https://fonts.gstatic.com" . $uri;
            
      $file = file_get_contents($requestPath);
      file_put_contents($filePath, $file); 
    }
    
    header($_SERVER["SERVER_PROTOCOL"] . " 200 OK");
    header("Access-Control-Allow-Origin: *");
    header("Cache-Control: public, max-age=31536000"); // needed for internet explorer
    header("Content-Type: font/woff2");
    header("Cross-Origin-Resource-Policy:	cross-origin");
    header("Content-Transfer-Encoding: Binary");
    header("Content-Length:".filesize($filePath));
    header('timing-allow-origin: *');
    header("content-security-policy-report-only: require-trusted-types-for 'script'");
    readfile($filePath);
    exit;  

}
?>